import React from 'react'
import Child from './child'
import './index.less'

export default class life extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      count: 0
    }
  }
  handleAdd=()=>{
    this.setState({
      count: this.state.count+1
    })
  }
  handleclick(){
    this.setState({
      count: this.state.count - 1
    })
  }
  render () {
    return <div className="border">
      <p>react </p>
      <p>{this.state.count}</p>
      <button onClick={this.handleAdd}>点击add</button>
      <button onClick={this.handleclick.bind(this)}>点击click</button>
      <p>{this.statecount}</p>
      <Child name='jack'/>
    </div>
  }
}